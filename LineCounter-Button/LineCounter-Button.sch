EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:LineCounter-Button-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X04 INOUT1
U 1 1 566A6A55
P 6800 3700
F 0 "INOUT1" H 6800 3950 50  0000 C CNN
F 1 "INOUT" V 6900 3700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x04" H 6800 3700 50  0001 C CNN
F 3 "" H 6800 3700 50  0000 C CNN
	1    6800 3700
	1    0    0    1   
$EndComp
$Comp
L CONN_02X02 SW1
U 1 1 566A6C4E
P 5250 3600
F 0 "SW1" H 5250 3750 50  0000 C CNN
F 1 "SW1" H 5250 3450 50  0000 C CNN
F 2 "mdn:SW_PUSH-12mm" H 5250 2400 50  0001 C CNN
F 3 "" H 5250 2400 50  0000 C CNN
	1    5250 3600
	0    -1   -1   0   
$EndComp
$Comp
L R R2
U 1 1 566A6D1D
P 5500 4250
F 0 "R2" V 5580 4250 50  0000 C CNN
F 1 "220" V 5500 4250 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5430 4250 50  0001 C CNN
F 3 "" H 5500 4250 50  0000 C CNN
	1    5500 4250
	0    1    1    0   
$EndComp
NoConn ~ 5300 3350
NoConn ~ 5200 3350
$Comp
L D D1
U 1 1 5684884B
P 6050 4250
F 0 "D1" H 6050 4350 50  0000 C CNN
F 1 "D1" H 6050 4150 50  0000 C CNN
F 2 "Diodes_SMD:MiniMELF_Handsoldering" H 6050 4250 50  0001 C CNN
F 3 "" H 6050 4250 50  0000 C CNN
	1    6050 4250
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 OUT1
U 1 1 56BDBA28
P 6800 3100
F 0 "OUT1" H 6800 3300 50  0000 C CNN
F 1 "OUT" V 6900 3100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x03" H 6800 3100 50  0001 C CNN
F 3 "" H 6800 3100 50  0000 C CNN
	1    6800 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 3850 6500 4250
Wire Wire Line
	5100 4250 5350 4250
Wire Wire Line
	6200 3650 6600 3650
Wire Wire Line
	6500 4250 6200 4250
Wire Wire Line
	5900 4250 5650 4250
Wire Wire Line
	5200 3850 5000 3850
Wire Wire Line
	5000 3850 5000 3550
Wire Wire Line
	5000 3550 4700 3550
Wire Wire Line
	5450 3750 6600 3750
Wire Wire Line
	5450 3750 5450 3850
Wire Wire Line
	5450 3850 5300 3850
Wire Wire Line
	5650 3550 6600 3550
Wire Wire Line
	6600 3850 6500 3850
Wire Wire Line
	5800 4250 5800 3000
Wire Wire Line
	5800 3000 6600 3000
Connection ~ 5800 4250
Wire Wire Line
	6200 3650 6200 3100
Wire Wire Line
	6200 3100 6600 3100
$Comp
L LED D2
U 1 1 56C1318B
P 4900 4250
F 0 "D2" H 4900 4350 50  0000 C CNN
F 1 "GREEN" H 4900 4150 50  0000 C CNN
F 2 "LEDs:LED_0805" H 4900 4250 50  0001 C CNN
F 3 "" H 4900 4250 50  0000 C CNN
	1    4900 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3550 5650 3000
Wire Wire Line
	5650 3000 4700 3000
Wire Wire Line
	4700 3000 4700 4250
Connection ~ 4700 3550
Wire Wire Line
	6600 3200 5650 3200
Connection ~ 5650 3200
$EndSCHEMATC
