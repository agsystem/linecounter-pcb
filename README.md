# README #

This is the design of add-on board for Raspi2 B+ to support line counter app (https://bitbucket.org/agsystem/linecounter)

### How do I get set up? ###

1. Open the Kicad files.
2. Burn into PCB.
3. Assembly components.
4. Plug into the Raspi board.
5. Done.

