EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:LineCounter-D2-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X08 IN1
U 1 1 566A05E2
P 7550 3100
F 0 "IN1" H 7550 3550 50  0000 C CNN
F 1 "IN" V 7650 3100 50  0000 C CNN
F 2 "Connect:RJ45_8" H 7550 3100 50  0001 C CNN
F 3 "" H 7550 3100 50  0000 C CNN
	1    7550 3100
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X04 OUT1
U 1 1 566A06C9
P 5250 4050
F 0 "OUT1" H 5250 4300 50  0000 C CNN
F 1 "OUT1" V 5350 4050 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x04" H 5250 4050 50  0001 C CNN
F 3 "" H 5250 4050 50  0000 C CNN
	1    5250 4050
	0    1    1    0   
$EndComp
$Comp
L CONN_01X04 OUT3
U 1 1 566A065A
P 5250 2200
F 0 "OUT3" H 5250 2450 50  0000 C CNN
F 1 "OUT3" V 5350 2200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x04" H 5250 2200 50  0001 C CNN
F 3 "" H 5250 2200 50  0000 C CNN
	1    5250 2200
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR01
U 1 1 56970F4E
P 7100 3850
F 0 "#PWR01" H 7100 3600 50  0001 C CNN
F 1 "GND" H 7100 3700 50  0000 C CNN
F 2 "" H 7100 3850 50  0000 C CNN
F 3 "" H 7100 3850 50  0000 C CNN
	1    7100 3850
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR02
U 1 1 56970F68
P 7100 2500
F 0 "#PWR02" H 7100 2350 50  0001 C CNN
F 1 "+3.3V" H 7100 2640 50  0000 C CNN
F 2 "" H 7100 2500 50  0000 C CNN
F 3 "" H 7100 2500 50  0000 C CNN
	1    7100 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 2500 7100 2750
Wire Wire Line
	7100 2750 7350 2750
Wire Wire Line
	7100 3850 7100 3450
Wire Wire Line
	7100 3450 7350 3450
$Comp
L CONN_01X04 OUT2
U 1 1 56970FDB
P 4400 3000
F 0 "OUT2" H 4400 3250 50  0000 C CNN
F 1 "OUT2" V 4500 3000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x04" H 4400 3000 50  0001 C CNN
F 3 "" H 4400 3000 50  0000 C CNN
	1    4400 3000
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR03
U 1 1 5697104D
P 5650 2400
F 0 "#PWR03" H 5650 2150 50  0001 C CNN
F 1 "GND" H 5650 2250 50  0000 C CNN
F 2 "" H 5650 2400 50  0000 C CNN
F 3 "" H 5650 2400 50  0000 C CNN
	1    5650 2400
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR04
U 1 1 5697106B
P 4600 2600
F 0 "#PWR04" H 4600 2350 50  0001 C CNN
F 1 "GND" H 4600 2450 50  0000 C CNN
F 2 "" H 4600 2600 50  0000 C CNN
F 3 "" H 4600 2600 50  0000 C CNN
	1    4600 2600
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR05
U 1 1 56971082
P 4950 3850
F 0 "#PWR05" H 4950 3600 50  0001 C CNN
F 1 "GND" H 4950 3700 50  0000 C CNN
F 2 "" H 4950 3850 50  0000 C CNN
F 3 "" H 4950 3850 50  0000 C CNN
	1    4950 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 2400 5650 2400
Wire Wire Line
	5100 3850 4950 3850
$Comp
L +3.3V #PWR06
U 1 1 569710B8
P 4850 2400
F 0 "#PWR06" H 4850 2250 50  0001 C CNN
F 1 "+3.3V" H 4850 2540 50  0000 C CNN
F 2 "" H 4850 2400 50  0000 C CNN
F 3 "" H 4850 2400 50  0000 C CNN
	1    4850 2400
	0    -1   -1   0   
$EndComp
$Comp
L +3.3V #PWR07
U 1 1 569710DA
P 4600 3450
F 0 "#PWR07" H 4600 3300 50  0001 C CNN
F 1 "+3.3V" H 4600 3590 50  0000 C CNN
F 2 "" H 4600 3450 50  0000 C CNN
F 3 "" H 4600 3450 50  0000 C CNN
	1    4600 3450
	-1   0    0    1   
$EndComp
$Comp
L +3.3V #PWR08
U 1 1 569710F1
P 5650 3850
F 0 "#PWR08" H 5650 3700 50  0001 C CNN
F 1 "+3.3V" H 5650 3990 50  0000 C CNN
F 2 "" H 5650 3850 50  0000 C CNN
F 3 "" H 5650 3850 50  0000 C CNN
	1    5650 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	5100 2400 4850 2400
Wire Wire Line
	4600 3150 4600 3450
Wire Wire Line
	5400 3850 5650 3850
Wire Wire Line
	7350 2850 5200 2850
Wire Wire Line
	5200 2850 5200 2400
Wire Wire Line
	4600 2850 4600 2600
Wire Wire Line
	7350 3050 4600 3050
Wire Wire Line
	7350 3250 5300 3250
Wire Wire Line
	5300 3250 5300 3850
Wire Wire Line
	7350 2950 5300 2950
Wire Wire Line
	5300 2950 5300 2400
Wire Wire Line
	7350 3150 4900 3150
Wire Wire Line
	4900 3150 4900 2950
Wire Wire Line
	4900 2950 4600 2950
Wire Wire Line
	7350 3350 5200 3350
Wire Wire Line
	5200 3350 5200 3850
$EndSCHEMATC
